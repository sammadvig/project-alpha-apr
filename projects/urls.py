from django.urls import path
from projects.views import (
    view_projects_list,
    view_project,
    create_project_view,
)


urlpatterns = [
    path("", view_projects_list, name="list_projects"),
    path("<int:id>/", view_project, name="show_project"),
    path("create/", create_project_view, name="create_project"),
]
